extern crate rand;

use std::*;
use std::fs::File;
use std::io::prelude::*;


const CIUDADES_CANTIDAD:u32=100;

#[derive(Debug,Copy, Clone)]
struct DistanciaPosicion {
    max: u32,
    min: u32,
    val: u32,
}

fn main() {
    let argumentos: Vec<String> = env::args().collect();
    let distancias_archivo;
//    println!("Args={:?}", argumentos);

    match argumentos.len(){                 //arg 0 es el ejecutable
        2 => {distancias_archivo=argumentos[1].to_string();},
        3 => {distancias_archivo=argumentos[1].to_string();},
        _ => {println!("Argumento 1 debe ser archivo de distancias."); process::exit(1)},
    }

    let mut ciudades_lista: Vec<u32>;                               //contiene las ciudades ordenadas


    //lee el archivo de distancias
    let mut f = File::open(distancias_archivo).expect("Archivo con distancias no se puede abrir!!");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("something went wrong reading the file");


    let mut tabu=vec![[0u32,0u32]]; tabu.pop();
    let matriz_distancias_size:usize=calc_matrix_size(CIUDADES_CANTIDAD);
    let mut matriz_distancias=vec![0u32;matriz_distancias_size];
    let mut cont=0;
    let mut row=1;
    for line in contents.lines(){
        let mut tamano:usize=0;
        for _i in line.split("\t"){
            tamano+=1;
        }
        tamano=(tamano as f32/10 as f32).floor() as usize;
        if tamano <= 0 { tamano=1; }                            //evitar error por no reservar nada
        let mut peores=vec![DistanciaPosicion {max:0,min:0,val:0}; tamano];
        let mut col=0;
        for i in line.split("\t"){
            let val:u32=i.parse::<u32>().unwrap();
            matriz_distancias[cont]=val;
            cont+=1;

            if row> 10 {
                let index = index_min(&mut peores);
                if peores[index].val < val {
                    peores[index].val = val;
                    let max;
                    let min;
                    if row > col {
                        max = row;
                        min = col;
                    } else {
                        max = col;
                        min = row;
                    }
                    peores[index].max = max;
                    peores[index].min = min;
                }
            }
            col += 1;
        }
        //for z in 0..peores.len(){
        //    if peores[z].val > 0{
        //        tabu.push([peores[z].max, peores[z].min]);
        //    }
        //}
        row+=1;
    }

    ciudades_lista=ciudades_voraz(&mut matriz_distancias);



    //HEURISTICA
    const ITERACIONES_MAX:u32 =10001;
    let mut iteraciones_reinicializacion=0;
    let mut mejor_solucion= ciudades_lista.to_vec();
    let mut mejor_distancia= calc_distancia_total(&mut mejor_solucion, &mut matriz_distancias);
    let mut mejor_iteracion=0;
    let mut contador_reinicializacion=0;
    let mut mejores_soluciones=vec![[0u32,0u32]]; mejores_soluciones.pop();

    println!("\
        RECORRIDO INICIAL\n\
        \tRECORRIDO: {}\n\
        \tCOSTE (km): {}\n\
        ", ciudades_lista.iter().
            fold(String::new(), |acc, e| {
                acc + &e.to_string() + " "
            })
        , mejor_distancia
    );

    for iteraciones in 1..ITERACIONES_MAX{
        if iteraciones_reinicializacion >=100 {
            iteraciones_reinicializacion=0;
            contador_reinicializacion+=1;
            ciudades_lista=mejor_solucion.to_vec();
            if contador_reinicializacion %10 == 0{
                for contador_rands in 0..10 {
                    //se determina aleatoriamente la parte del vector a aleatorizar
                    //no interesa hacerlo con mucho porque se pierde el trabajo realizado
                    let rango = 10;               //cantidad de posiciones que se van a semi aleatorizar en el vector
                    let comienzo: usize = contador_rands * 10 + (contador_reinicializacion%2)*5;
                    let fin;
                    if comienzo + rango > ciudades_lista.len() - 1 -(contador_reinicializacion%2)*5 {
                        fin = comienzo + rango - 1 -(contador_reinicializacion%2)*5;
                    } else {
                        fin = comienzo + rango;
                    }

                    let mut fraccion_antes: Vec<u32> = vec![0u32]; fraccion_antes.pop();
                    for pos in 0..comienzo {
                        fraccion_antes.push(ciudades_lista[pos]);
                    }
                    let mut fraccion_despues: Vec<u32> = vec![0u32]; fraccion_despues.pop();
                    for pos in fin..ciudades_lista.len() {
                        fraccion_despues.push(ciudades_lista[pos]);
                    }
                    let mut fraccion: Vec<u32> = vec![0u32]; fraccion.pop();
                    for pos in comienzo..fin {
                        fraccion.push(ciudades_lista[pos]);
                    }
                    let mut vec: Vec<u32> = fraccion.to_vec();
//                    if contador_reinicializacion %3 ==0{
                    if 0 ==0{               //determinista
                        let (bmax,bmin,_)=calc_mejor_intercambio(&mut fraccion, &mut matriz_distancias, &mut vec![[0u32,0u32]]);
                        vec=permuta_posiciones_invirtiendo(&mut fraccion, bmax as usize, bmin as usize);
                    }else{
                        let mut mejor_val = u32::max_value();
                        let mut probado = vec![fraccion.to_vec()];
                        for _pos in 0..fraccion.len() {
                            //reordena el array aleatoriamente
                            let mut fraccion_rand = fraccion.to_vec();
                            use rand::{thread_rng, Rng};
                            thread_rng().shuffle(&mut fraccion_rand);
                            while probado.contains(&fraccion_rand) {             //para no volver a probar lo mismo, es factorial pero posible
                                thread_rng().shuffle(&mut fraccion_rand);
                            }
                            probado.push(fraccion_rand.to_vec());
                            let val=calc_distancia_total(&mut fraccion_rand, &mut matriz_distancias);
                            if val < mejor_val {
                                vec = fraccion_rand.to_vec();
                                mejor_val = val;
                            }
                        }
                    }
                    ciudades_lista=fraccion_antes.to_vec();
                    ciudades_lista.append(& mut vec);
                    ciudades_lista.append(& mut fraccion_despues);
                }
            }else{
//                tabu=vec![[0u32,0u32]]; tabu.pop();
            }
            println!("\
                ***************\n\
                REINICIO: {}\n\
                ***************\n"
            , contador_reinicializacion
            );
        }

        let (bmax,bmin,best_temporal)=
            calc_mejor_intercambio(&mut ciudades_lista, &mut matriz_distancias, &mut tabu);

        tabu.push([bmax,bmin]);
        //aumentada la cantidad
        if tabu.len() > (CIUDADES_CANTIDAD*10) as usize{
            tabu.remove(0);
        }

        ciudades_lista=permuta_posiciones_invirtiendo(&mut ciudades_lista, bmax as usize, bmin as usize);
        if best_temporal < mejor_distancia{
            mejor_solucion=ciudades_lista.to_vec();
            mejor_distancia=best_temporal;
            mejor_iteracion=iteraciones;
            iteraciones_reinicializacion=0;
            mejores_soluciones.push([mejor_distancia,iteraciones]);
        }else{
            iteraciones_reinicializacion+=1;
        }


        //por eficiencia
        let tmp_str=format!("\
            ITERACION: {}\n\
            \tINTERCAMBIO: ({}, {})\n\
            \tRECORRIDO: {}\n\
            \tCOSTE (km): {}\n\
            \tITERACIONES SIN MEJORA: {}\n\
            \tLISTA TABU:\n{}\
            ", iteraciones
            , bmax , bmin
            , ciudades_lista.iter().
            fold(String::new(), |acc, e| {
                acc + &e.to_string() + " "
            })
            , best_temporal
            , iteraciones_reinicializacion
            , tabu.iter()
                 .fold(String::new(), |acc, &[i, j]| {
                     acc + "\t" + &i.to_string() + " " + &j.to_string() + "\n"
                 })
        );
        println!("{}",tmp_str);
    }

    //añadido para ver fácilmente los cambios de MJ habidos
    for pos in 0..mejores_soluciones.len(){
        println!("\tMJ[{}]={}, en {} iteraciones",pos,mejores_soluciones[pos][0], mejores_soluciones[pos][1]);
    }

    println!("\
        \nMEJOR SOLUCION: \n\
        \tRECORRIDO: {}\n\
        \tCOSTE (km): {}\n\
        \tITERACION: {}\
        ", mejor_solucion.iter().
            fold(String::new(), |acc, e| {
                acc + &e.to_string() + " "
           })
        , mejor_distancia
        , mejor_iteracion
    );

    for pos in 1..mejor_solucion.len(){
        let ci=pos as u32;
        if ! mejor_solucion.contains(&ci){
            println!("La solucion no contiene a la ciudad {}",pos);
        }
    }
}


fn calc_mejor_intercambio(ciudades_lista:&mut Vec<u32>, matriz_distancias:&mut Vec<u32>, tabu:&mut Vec<[u32;2]>) -> (u32,u32,u32){
    let mut best_temporal=u32::max_value();
    let mut bmax:u32=0;
    let mut bmin:u32=0;
    for i in 0..ciudades_lista.len(){
        for j in 0..i {
            let max:usize;
            let min:usize;
            if i > j {
                max = i as usize;
                min = j as usize;
            } else {
                max = j as usize;
                min = i as usize;
            }
            if i == j || tabu.contains(&[max as u32, min as u32]) {
                continue;
            }

            let distancia_permutacion: u32;
            distancia_permutacion = calc_distancia_total(
            &mut permuta_posiciones_invirtiendo(ciudades_lista, max, min)
            , matriz_distancias);

            if distancia_permutacion < best_temporal {
                best_temporal=distancia_permutacion;
                bmax=max as u32;
                bmin=min as u32;
            }
        }
    }
    (bmax, bmin, best_temporal)
}



fn index_min(vec: &mut [DistanciaPosicion]) -> usize{
    let mut index:usize=0;
    let mut min:u32=u32::max_value();
    for z in 0..vec.len(){
        if vec[z].val == 0{
            return z
        }else if vec[z].val < min{
            index=z;
            min=vec[z].val;
        }
    }
    index
}

fn permuta_posiciones_invirtiendo(solucion: &mut [u32], i:usize, j:usize) -> Vec<u32> {
    let mut permutado= solucion.to_vec();
    for index in j..(i+1){
        permutado[index] = solucion[i+j-index];
    }

    permutado
}

//fn permuta_posiciones(solucion: &mut [u32], i:usize, j:usize) -> Vec<u32> {
//    let mut permutado= solucion.to_vec();
//    permutado[i]=solucion[j];
//    permutado[j]=solucion[i];
//
//    permutado
//}
fn calc_matrix_index(i:u32, j:u32) -> usize {
    ((i + 1) * i / 2 + j) as usize
}

fn calc_distancia_inicio_fin(solucion: &mut [u32], matriz_distancias: &mut [u32]) -> u32{
    calc_distancia(matriz_distancias, 0, solucion[0])
        + calc_distancia(matriz_distancias, 0, solucion[(solucion.len()-1) as usize])
}
//fn calc_distancia_interna(solucion: &mut [u32], matriz_distancias: &mut [u32]) -> u32{
//    let mut distancia_total: u32= 0;
//    let max:usize=solucion.len()-1;
//    let mut i:usize=0;
//    while i< max{
//        distancia_total += calc_distancia(matriz_distancias, solucion[i], solucion[i+1]);
//        i+=1;
//    }
//    distancia_total
//}
fn calc_distancia_total(solucion: &mut [u32], matriz_distancias: &mut [u32]) -> u32{
    let mut distancia_total: u32= calc_distancia_inicio_fin(solucion, matriz_distancias);
    let max:usize=solucion.len()-1;
    let mut i:usize=0;
    while i< max{
        distancia_total += calc_distancia(matriz_distancias, solucion[i], solucion[i+1]);
        i+=1;
    }
    distancia_total
}
fn calc_distancia(matriz_distancias: &mut [u32], i:u32, j:u32) -> u32{
    let row;
    let col;
    if i>j{
        row=i;
        col=j;
    }else if j>i {
        row=j;
        col=i;
    }else{
        return 0
    }

    matriz_distancias[calc_matrix_index(row-1,col)]
}


fn calc_matrix_size(max:u32) -> usize{
    let mut tam:u32=0;
    for i in 1..max{
        tam+=i;
    }
    return tam as usize
}

fn ciudades_voraz(matriz_distancias: &mut [u32]) -> Vec<u32>{
    let mut vec: Vec<u32>=vec![0u32];

    //se recorre la columna 0 y se selecciona el menor valor
    //luego se hace lo mismo con la columna del valor anterior +1, hasta terminar
    let mut index=0;
    for _i in 0..CIUDADES_CANTIDAD-1{
        let mut min=u32::max_value();
        let mut nindex=index;
        for j in 0..CIUDADES_CANTIDAD {
            if ! vec.contains(&j) && j!=index {
                let val=calc_distancia(matriz_distancias,index,j);
                if val < min{
                    min=val;
                    nindex=j;
                }
            }
        }
        if nindex==index{
            println!("ERROR index==nindex=={}",index);
            process::exit(2);
        }
        index=nindex;
        vec.push(nindex);
    }

    vec.remove(0);              //elimina el valor 0 inicial, para no trabajar con un vector vacío
    vec.to_vec()
}
